<?php

/**
 * BinaryToHex
 *
 * PHP version 5.3.1
 *
 * @category Static
 * @package  BinaryToHex
 * @author   John Merigliano <automato@automaton.host-ed.me>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://automaton.host-ed.me/
 *
 */

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>BinaryToHex</title>
</head>

<body>
    <div class="widget">
        <h2>Conversion: Binary to Hexadecimal</h2>
        <p>Entering a binary sequence of numbers (ex: 001001110)<br>at the prompt will produce an equivalent hexadecimal value. Example: 001001110 entered = 04E</p>

        <!-- prompt user for a value -->
        <form id="user_value" method="post" action="">
            <p>
                <label for="user_value">Enter a binary sequence of numbers</label>
                <br>
                <input name="binary_num" id="binary_num" type="number" autocomplete="off">
                <input name="send" id="send" type="submit" value="Enter">
            </p>
        </form>

        <!-- display the results -->
        <?php
            if (isset($_POST["binary_num"])) {
                require_once 'convert_binary_to_hex.php';
            }
        ?>
    </div><!-- /widget -->
</body>
</html>