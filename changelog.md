# Change Log

Project version: 1.0
====================

## 1.0 - 2016-03-27
### Updated
- separated validation into separate function
- refactored html
- added install instructions to readme

### Added
- added main method to script and removed from html
- added this changelog


## 1.0 - 2014-11-01
### Added
- initial project commited to vc