# Binary To Hex

## Overview
This script converts a numerical binary sequence to a hexadecimal value.

For example `0110101100110 -> 0D66`

## Setup
To get this running, you will need a server environment with PHP. You can use [MAMP](https://www.mamp.info/en/). Download this repo and add it to your HTDOCS. Open `index.php`.
