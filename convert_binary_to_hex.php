<?php

/**
 * BinaryToHex
 *
 * PHP version 5.3.1
 *
 * @category Static
 * @package  BinaryToHex
 * @author   John Merigliano <jmerigliano@gmail.com>
 * @license  http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link     http://lautomator.github.io/
 *
 *
 * This script converts a numerical binary sequence to a hexadecimal value.
 * For ex: 0110101100110 -> 0D66
 *
 * revised: 03-2016
 *
 */

// value posted by the user
$user_value = $_POST["binary_num"];


function validateInput($entered) {
    /** validates user input:
     * - must be a number
     * - must be 0's or 1's only
     * Returns True or False <boolean>
    **/

    $len = strlen($entered);
    $i = 0;
    // assume invalid data has been entered
    $ok = False;

    while ($i < $len) {
        if ($entered[$i] < 0 || $entered[$i] > 1) {
            $ok = False;
            break;
        } else {
            $ok = True;
        }
        $i += 1;
    }

    return $ok;
}


function binaryToHex($entered) {
    /**
     * Converts the string of numbers to a hexadecimal value.
     * Takes in the user value <int>.
     * Returns hexadecimal value <string>.
     * The Hexadecimal value is derived from clusters of 4 values
     * (ie, 0010). If there are not groups of 4, 0's must be added
     * to the beginning of the entry.
     * add 0's to the beginning of the entry. For example:
     * 010 entered becomes 0010.
     * determine the length of the string
    **/

    $len = strlen($entered);

    // add zeros to beginning of value, if needed
    // if len % 4 = .25, there is 1 extra number in the string
    // add 3 zeros at the beginning of the string
    if ($len % 4 == 1) {
        $binary_value = '000' . $entered;
        // if len % 4 = .5, there are 2 extra numbers in the string
        // add 2 zeros
    } elseif ($len % 4 == 2) {
            $binary_value =  '00'. $entered;
        // if len % 4 = .75, there are 3 extra numbers in the string
        // add 1 zero
    } elseif ($len % 4 == 3) {
            $binary_value =  '0'. $entered;

        // otherwise there is no remainder
        // return the original string
    } else {
        $binary_value = $entered;
    }

    // convert the binary units into Hex values
    // cycle through the string of numbers
    // convert groups of 4 to hex values
    for ($j = 0; $j < strlen($binary_value); $j += 4) {
        // a group of four digits
        $binary_unit = substr($binary_value, $j, 4);

        // method will return any or all of the following conditions
        switch ($binary_unit) {
        case '0000':
            echo '0';
            break;
        case '0001':
            echo '1';
            break;
        case '0010':
            echo '2';
            break;
        case '0011':
            echo '3';
            break;
        case '0100':
            echo '4';
            break;
        case '0101':
            echo '5';
            break;
        case '0110':
            echo '6';
            break;
        case '0111':
            echo '7';
            break;
        case '1000':
            echo '8';
            break;
        case '1001':
            echo '9';
            break;
        case '1010':
            echo 'A';
            break;
        case '1011':
            echo 'B';
            break;
        case '1100':
            echo 'C';
            break;
        case '1101':
            echo 'D';
            break;
        case '1110':
            echo 'E';
            break;
        case '1111':
            echo 'F';
            break;
        }
    }
}


function main($entered) {
    // when the user posts the application runs
    $ok = validateInput($entered);

    if ($ok) {
        echo "<p>You entered: <strong>$entered</strong></p>
        <p>Hex value is: <strong>";
        // call the function to convert the user entry
        // to a hexadecimal number
        binaryToHex($entered);
        echo "</strong></p>";
    } else {
        unset($entered);
        echo '<p class="alert">Enter 0\'s or 1\'s only.</p>';
    }
}

main($user_value);